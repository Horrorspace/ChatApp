import {IConfig} from '@interfaces/IConfig';


export const config: IConfig = {
    host: 'localhost',
    port: 3007,
    encryption: false
}