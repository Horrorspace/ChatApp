export enum AuthActTypes {
    setAuth = 'setAuth',
    clearAuth = 'clearAuth',
    setToken = 'setToken',
    clearToken = 'clearToken',
    loginThunk = 'loginThunk',
    registerThunk = 'registerThunk',
    logoutThunk = 'logoutThunk',
    getTokenThunk = 'getTokenThunk',
    getUserThunk = 'getUserThunk'
}
