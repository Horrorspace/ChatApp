export enum MessagesActTypes {
    setMessages = 'setMessages',
    addMessage = 'addMessage',
    setReadMessage = 'setReadMessage',
    deleteMessage = 'deleteMessage',
    getAllMessagesThunk = 'getAllMessagesThunk',
    sendNewMessageThunk = 'sendNewMessageThunk',
    setReadMessageThunk = 'setReadMessageThunk',
    deleteMessageThunk = 'deleteMessageThunk'
}
